package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "peliscrud")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PelisCrud.findAll", query = "SELECT p FROM PelisCrud p"),
    @NamedQuery(name = "PelisCrud.findById", query = "SELECT p FROM PelisCrud p WHERE p.id = :id"),
    @NamedQuery(name = "PelisCrud.findByPeliName", query = "SELECT p FROM PelisCrud p WHERE p.peliName = :peliName"),
    @NamedQuery(name = "PelisCrud.findByPeliYear", query = "SELECT p FROM PelisCrud p WHERE p.peliYear = :peliYear"),
    @NamedQuery(name = "PelisCrud.findByPeliReview", query = "SELECT p FROM PelisCrud p WHERE p.peliReview = :peliReview"),
    @NamedQuery(name = "PelisCrud.findByPeliCast", query = "SELECT p FROM PelisCrud p WHERE p.peliCast = :peliCast"),
    @NamedQuery(name = "PelisCrud.findByPeliCategory", query = "SELECT p FROM PelisCrud p WHERE p.peliCategory = :peliCategory"),
    @NamedQuery(name = "PelisCrud.findByPeliDirector", query = "SELECT p FROM PelisCrud p WHERE p.peliDirector = :peliDirector")})
public class PelisCrud implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "peli_name")
    private String peliName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "peli_year")
    private int peliYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "peli_review")
    private String peliReview;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "peli_cast")
    private String peliCast;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "peli_category")
    private String peliCategory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "peli_director")
    private String peliDirector;

    public PelisCrud() {
    }

    public PelisCrud(Integer id) {
        this.id = id;
    }

    public PelisCrud(Integer id, String peliName, int peliYear, String peliReview, String peliCast, String peliCategory, String peliDirector) {
        this.id = id;
        this.peliName = peliName;
        this.peliYear = peliYear;
        this.peliReview = peliReview;
        this.peliCast = peliCast;
        this.peliCategory = peliCategory;
        this.peliDirector = peliDirector;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeliName() {
        return peliName;
    }

    public void setPeliName(String peliName) {
        this.peliName = peliName;
    }

    public int getPeliYear() {
        return peliYear;
    }

    public void setPeliYear(int peliYear) {
        this.peliYear = peliYear;
    }

    public String getPeliReview() {
        return peliReview;
    }

    public void setPeliReview(String peliReview) {
        this.peliReview = peliReview;
    }

    public String getPeliCast() {
        return peliCast;
    }

    public void setPeliCast(String peliCast) {
        this.peliCast = peliCast;
    }

    public String getPeliCategory() {
        return peliCategory;
    }

    public void setPeliCategory(String peliCategory) {
        this.peliCategory = peliCategory;
    }

    public String getPeliDirector() {
        return peliDirector;
    }

    public void setPeliDirector(String peliDirector) {
        this.peliDirector = peliDirector;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PelisCrud)) {
            return false;
        }
        PelisCrud other = (PelisCrud) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.PelisCrud[ id=" + id + " ]";
    }
    
}
