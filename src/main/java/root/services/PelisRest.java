package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.PelisCrud;

@Path("/pelis")
public class PelisRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("PelisCrud_PU");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        em = emf.createEntityManager();
        List<PelisCrud> lista = em.createNamedQuery("PelisCrud.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarPorId(@PathParam("id") Integer id) {
        em = emf.createEntityManager();
        PelisCrud peli = em.find(PelisCrud.class, id);
        return Response.ok(200).entity(peli).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String agregarPelicula(PelisCrud peliNueva) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(peliNueva);
        em.getTransaction().commit();
        return "Pelicula guardada";
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarPelicula(PelisCrud peliUpdate) {
        String resultado = "";
        em = emf.createEntityManager();
        em.getTransaction().begin();
        peliUpdate = em.merge(peliUpdate);
        em.getTransaction().commit();
        return Response.ok(peliUpdate).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarPorId(@PathParam("id") Integer id) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        PelisCrud eliminarPeli = em.getReference(PelisCrud.class, id);
        em.remove(eliminarPeli);
        em.getTransaction().commit();
        return Response.ok("Pelicula eliminada").build();
    }

}
